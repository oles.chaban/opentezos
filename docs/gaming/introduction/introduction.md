---
id: introduction
disable_pagination: true
title: Introduction
slug: /gaming
author: Mathias Hiron
---

## Games on Tezos

The Tezos SDK invites developers to discover the future of Web3 gaming with a complete kit that empowers game developers with the ability to:

**Connect to a Tezos wallet** – Using the wallet-pairing feature of the SDK, you can allow users to authenticate using their Tezos blockchain credentials. Users can securely authenticate themselves in a game, sign blockchain transactions, gain access to all on-chain assets, and use cryptocurrency to purchase services, features, or in-game assets.

**Utilize data on the blockchain**  – The SDK enables game developers to capture data on the Tezos blockchain, such as checking which features of the game or in-game assets the user owns. Importantly, users can manage the storage of their assets via smart contracts or call off-chain views, creating access to any data stored in the blockchain.

**Call smart contracts** – Games will be able to generate calls to Tezos smart contracts, to be signed by the user– opening the limitless possibilities of smart contracts on Tezos. From simple transactions such as purchasing extra features using cryptocurrency, to generating in-game assets based on achievements, this feature will allow users to trade on public marketplaces, transfer in-game assets, or participate in multi-game challenges.

**True ownership of in-game assets** – Through the SDK, users will be able to own and easily verify the authenticity of the in-game assets via the Tezos blockchain. Signing data through smart contracts enables games to prompt users to verify the authenticity of data signed by other users. 

The Tezos SDK supports Desktop, Android, iOS and browsers. Beyond allowing game developers to interact with the Tezos blockchain, this SDK is a helpful resource for developing any Tezos decentralized application (dApp).

